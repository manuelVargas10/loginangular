import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-input-component',
  templateUrl: './input-component.component.html',
  styleUrls: ['./input-component.component.css']
})
export class InputComponentComponent implements OnInit {
  @Input() typeInput = '';
  @Input() placeholderInput = '';

  constructor() { }

  ngOnInit() {
  }

}
