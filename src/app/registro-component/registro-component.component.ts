import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-registro-component',
  templateUrl: './registro-component.component.html',
  styleUrls: ['./registro-component.component.css']
})
export class RegistroComponentComponent implements OnInit {
  @Input() functionEditEstadoParams;

  constructor() { }

  ngOnInit() {
  }

}
