import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-button-component',
  templateUrl: './button-component.component.html',
  styleUrls: ['./button-component.component.css']
})
export class ButtonComponentComponent implements OnInit {
  @Input() texButton;
  @Input() typeButton;
  @Input() iconButton;

  constructor() { }

  ngOnInit() {
  }
  valdiateButton(element) {
    switch (element) {
      case 'primary':
        return 'btn btn-primary';
        break;
      case 'danger':
        return 'btn btn-danger';
        break;
      case 'secondary':
        return 'btn btn-secondary';
        break;
      case  'success':
        return 'btn btn-success';
        break;
    }
  }

}
