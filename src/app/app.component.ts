import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'login';
  estadoVista = true;

  functionEditEstado() {
   this.estadoVista = !this.estadoVista;
  }
}
